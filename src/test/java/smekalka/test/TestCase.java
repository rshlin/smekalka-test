package smekalka.test;

import org.junit.Test;

import java.util.Collection;

public class TestCase {
    @Test
    public void testGroups() {
        Collection<TokenGroup> tokenGroups = new StringLineTokenGrouper(new Tokenizer()).aggregateTokenGroups("раз,2, три,\n" +
                "елочка, гори!\n" +
                "три, 2, раз...\n" +
                "лысый дикобраз\n" +
                "***");
        tokenGroups.forEach(tokenGroup -> {
            System.out.println("*");
            tokenGroup.getLines().stream().map(lineWithTokens -> lineWithTokens.line)
                    .forEach(System.out::println);
        });
    }
}
