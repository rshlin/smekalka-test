package smekalka.test;

import com.google.common.collect.Sets;

import java.util.Set;

public class TokenGroup {
    private final Set<StringLineWithTokens> lines;

    public TokenGroup(StringLineWithTokens firstLine) {
        this(Sets.newHashSet(firstLine));
    }

    public TokenGroup(Set<StringLineWithTokens> lines) {
        this.lines = lines;
    }

    public Set<StringLineWithTokens> getLines() {
        return lines;
    }
}
