package smekalka.test;

import java.util.Set;

public class StringLineWithTokens {
    final String line;
    final Set<String> tokens;

    public StringLineWithTokens(String line, Set<String> tokens) {
        this.line = line;
        this.tokens = tokens;
    }
}
