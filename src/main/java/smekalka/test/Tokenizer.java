package smekalka.test;

import com.google.common.base.Splitter;
import com.google.common.collect.Sets;

import java.util.Set;
import java.util.regex.Pattern;

public class Tokenizer {

    public static final Splitter SPLITTER = Splitter.on(Pattern.compile("[ ,.!]+")).trimResults().omitEmptyStrings();

    public Set<String> tokenizeLine(String line) {
        return Sets.newHashSet(SPLITTER.split(line));
    }
}
