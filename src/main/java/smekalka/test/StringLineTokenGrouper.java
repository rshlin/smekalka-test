package smekalka.test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class StringLineTokenGrouper {
    private final Tokenizer tokenizer;

    public StringLineTokenGrouper(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }
    private BiFunction<Map<Integer, TokenGroup>, StringLineWithTokens, Map<Integer, TokenGroup>> accumulateTokens = (result, stringLineWithTokens) -> {
        result.merge(stringLineWithTokens.tokens.hashCode(), new TokenGroup(stringLineWithTokens), (tokenGroup, tokenGroup2) -> {
            tokenGroup.getLines().addAll(tokenGroup2.getLines());
            return tokenGroup;
        });
        return result;
    };

    private BinaryOperator<Map<Integer, TokenGroup>> combineResults = (map1, map2) -> {
        map1.putAll(map2);
        return map1;
    };
    private Pattern NO_WORD_PATTERN = Pattern.compile("^[*]+$");

    private Predicate<String> hasWord = line -> !NO_WORD_PATTERN.matcher(line).matches();

    public Collection<TokenGroup> aggregateTokenGroups(String text) {
        return Arrays.stream(text.split("\\R"))
                .parallel()
                .map(String::trim)
                .filter(line -> !line.isEmpty())
                .filter(hasWord)
                .map(line -> new StringLineWithTokens(line, tokenizer.tokenizeLine(line)))
                .reduce(new ConcurrentHashMap<>(), accumulateTokens, combineResults)
                .values();
    }
}
